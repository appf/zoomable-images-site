---
title: "Robertson Greenhouse Compound"
date: 2021-07-28T17:24:38+10:00
draft: false
summary: "Drone orthomosaic scan of RN Roberson Greenhouse Compound in Oct 2019"
image: "https://b2-anu.appf.org.au/file/acacia/zoomable-images/Robertson_greenhouse_area_Oct_2019/tn_original.jpg"
openseadragon:
    url: "https://b2-anu.appf.org.au/file/acacia/zoomable-images/Robertson_greenhouse_area_Oct_2019/image.dzi"
---


