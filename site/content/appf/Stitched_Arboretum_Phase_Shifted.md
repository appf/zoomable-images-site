---
title: "Arboretum Test Stitch (Phase Shift method)"
date: 2021-07-28T16:45:56+10:00
draft: false
summary: "A test stitch for the National Arboretum Gigavision Camera."
image: https://b2-anu.appf.org.au/file/acacia/zoomable-images/Stitched_Arboretum_Phase_Shifted/image_files/tn_original.jpg
openseadragon:
    url: "https://b2-anu.appf.org.au/file/acacia/zoomable-images/Stitched_Arboretum_Phase_Shifted/image.dzi"
---
A test stitch for the National Arboretum Gigavision Camera. Uses phase shift to align images to one another. Doesn't perform any colour balancing.
