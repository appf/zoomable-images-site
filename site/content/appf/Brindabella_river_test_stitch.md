---
title: "Brindabella River Test Stitch"
date: 2021-07-28T16:46:14+10:00
draft: false
summary: "Test stitch on images taken at Brindabella river"
image: "https://b2-anu.appf.org.au/file/acacia/zoomable-images/Brindabella_river_test_stitch/tn_original.jpg"
openseadragon:
    url: "https://b2-anu.appf.org.au/file/acacia/zoomable-images/Brindabella_river_test_stitch/image.dzi"
---

