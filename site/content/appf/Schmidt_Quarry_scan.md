---
title: "Schmidt Quarry Scan"
date: 2021-07-28T16:47:23+10:00
draft: false
summary: "Drone orthomosaic of Schmidt Quarry"
image: "https://b2-anu.appf.org.au/file/acacia/zoomable-images/Schmidt_Quarry_scan/tn_original.jpg"
openseadragon:
    url: "https://b2-anu.appf.org.au/file/acacia/zoomable-images/Schmidt_Quarry_scan/image.dzi"
---

