---
title: "Orion Nebula"
date: 2021-07-28T15:48:47+10:00
draft: false
image: https://b2-anu.appf.org.au/file/acacia/zoomable-images/Orion_Nebula/tn_original.jpg
summary: "Orion Nebula (Messier 42)"
openseadragon:
    url: "https://b2-anu.appf.org.au/file/acacia/zoomable-images/Orion_Nebula/image.dzi"
---

Created using 520 different Hubble exposures taken in multiple wavelengths of light, this mosaic contains over one billion pixels. Hubble imaged most of the nebula, but ground-based images were used to fill in the gaps in its observations. The orange color in the image can be attributed to hydrogen, green represents oxygen, and the red represents both sulfur and observations made in infrared light.

Source: [NASA Messier Catalog](https://www.nasa.gov/feature/goddard/2017/messier-42-the-orion-nebula)