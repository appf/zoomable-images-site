---
title: "Carina Nebula"
date: 2021-07-28T15:27:45+10:00
draft: false
image: https://b2-anu.appf.org.au/file/acacia/zoomable-images/Carina/tn_original.jpg
summary: "Carina Nebula"
openseadragon:
    url: "https://b2-anu.appf.org.au/file/acacia/zoomable-images/Carina/image.dzi"
---

The most detailed image of the Carina Nebula ever taken (as of 2009). The controlled color image is a composite of 48 high-resolution frames taken by the Hubble Space Telescope in 2007.

Source: [NASA APOD](https://apod.nasa.gov/apod/ap090524.html)



